import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';

import Button from 'components/Button';
import Dialog from 'components/Dialog';

export default class DataTable extends Component {
  static propTypes = {
    dataHead: PropTypes.array,
    dataBody: PropTypes.array,
    onClick: PropTypes.func,
    sortBy: PropTypes.func,
  }

  render() {
    const { dataHead, dataBody, onClick, sortBy } = this.props;

    return (
      <Table
        fixedHeader={ true }
        selectable={ false }
        multiSelectable={ false }
      >
        <TableHeader displaySelectAll={ false } adjustForCheckbox={ false }>
          <TableRow>
            {dataHead.map((row) => (
              <TableHeaderColumn key={ row.id } tooltip={ row.value }>
                <Link to='#' onClick={ () => sortBy(row.value) }>
                  <b>{ row.value.toUpperCase() }</b>
                </Link>
              </TableHeaderColumn>
            ))}
          </TableRow>
        </TableHeader>

        <TableBody
          displayRowCheckbox={ false }
          deselectOnClickaway={ true }
          showRowHover={ true }
          stripedRows={ false }
        >
          {dataBody.map((row) => (
            <TableRow key={ row.id }>
              <TableRowColumn>{ row.id }</TableRowColumn>
              <TableRowColumn>
                <Link to={ `/post/${ row.id }` }>
                  { row.todo_title }
                </Link>
              </TableRowColumn>
              <TableRowColumn>{ row.todo_category }</TableRowColumn>
              <TableRowColumn>{ row.todo_sub_category }</TableRowColumn>
              <TableRowColumn>
                <Button typeButton='iconLink' to={ `/post/${ row.id }/edit` } iconName='edit' />
              </TableRowColumn>
              <TableRowColumn>
                <Dialog title='Сonfirm deletion' onClick={ () => onClick(row.id) } secondaryClass={ true } typeButton='icon' iconName='delete'>
                  <p>Press the &laquo;Cancel&raquo; button until it&lsquo;s too late :)</p>
                </Dialog>
              </TableRowColumn>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    );
  }
}
