import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Dialog } from 'material-ui';

import Button from 'components/Button';

import css from 'style/components/Dialog';

const styles = {
  button: {
    margin: '0px 12px 12px',
    marginTop: 0,
  },
};

export default class DialogExample extends React.Component {
  static propTypes = {
    children: PropTypes.any,
    paramsButton: PropTypes.object,
    onClick: PropTypes.func,
    close: PropTypes.func,
    disabled: PropTypes.bool,
    open: PropTypes.bool,
    slogan: PropTypes.bool,
    title: PropTypes.string,
    buttonName: PropTypes.string,
  }

  state = {
    open: false,
  }

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { title, onClick, disabled, buttonName, slogan, children, ...params } = this.props;
    const actions = [
      <Button
        typeButton='flat'
        label='Send'
        primaryClass={ true }
        disabledClass={ disabled }
        onClick={ onClick }
      />,
      <Button
        typeButton='flat'
        style={ styles.button }
        label='Cancel'
        onClick={ this.handleClose }
      />,
    ];

    return (
      <span>
        {!slogan ?
          <Button onClick={ this.handleOpen } { ...params }>{ buttonName }</Button>
          :
          <ul className={ cx(css.buttonGroup, css.buttonGroup_middle) }>
            <li className={ css.buttonGroup__item }>
              <Button typeButton='circle' iconName='add' onClick={ this.handleOpen } />
            </li>
            <li className={ css.buttonGroup__item }>
              <h3 className={ css.buttonGroup__title }>{ title }</h3>
            </li>
          </ul>}

        <Dialog
          title={ title }
          actions={ actions }
          modal={ false }
          open={ this.state.open }
          onRequestClose={ this.handleClose }
        >
          { children }
        </Dialog>
      </span>
    );
  }
}
