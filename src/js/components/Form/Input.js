import React from 'react';
import PropTypes from 'prop-types';
import { TextField } from 'material-ui';

const Input = ({ label, name, value, onChange, ...input }) => (
  <TextField
    floatingLabelText={ label }
    fullWidth={ true }
    name={ name }
    value={ value }
    onChange={ onChange }
    { ...input }
  />
);

Input.propTypes = {
  name: PropTypes.string,
  label: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
};

export default Input;
