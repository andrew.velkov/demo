import React from 'react';
import PropTypes from 'prop-types';

import Button from 'components/Button';
import Input from 'components/Form/Input';

import css from 'style/pages/Main';

const Login = ({ email, password, onChange, onSubmit }) => (
  <form onSubmit={ onSubmit }>
    <Input label='Email *' type='text' name='email' value={ email } onChange={ onChange } />
    <Input label='Password *' type='password' name='password' value={ password } onChange={ onChange } />
    <div className={ css.buttonGroup }>
      <Button type='submit' primaryClass={ true }>Login</Button>
    </div>
  </form>
);

Login.propTypes = {
  email: PropTypes.string,
  password: PropTypes.string,
  onChange: PropTypes.func,
  onSubmit: PropTypes.func,
};

export default Login;
