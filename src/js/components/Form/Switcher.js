import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Switcher extends Component {
  static propTypes = {
    input: PropTypes.object,
  };

  state = {
    isChecked: false,
  };

  componentWillReceiveProps(nextProps) {
    const { input: { value } } = nextProps;
    this.setState({
      isChecked: value,
    });
  }

  toggleExclude = (e) => {
    const { input: { onChange } } = this.props;
    onChange(e.target.checked);
    this.setState({
      isChecked: e.target.checked,
    });
  };

  render() {
    const { isChecked } = this.state;

    return (
      <div>
        Exclude these countries
        <input type='checkbox' onChange={ (e) => this.toggleExclude(e) } checked={ isChecked } />
      </div>
    );
  }
}
