import React from 'react';
import { NavLink } from 'react-router-dom';
import cx from 'classnames';

import PAGES from 'config/pages';
import css from 'style/components/Header';

const Header = () => (
  <header className={ css.header }>
    <section className={ cx(css.header__wrap, css.header__wrap_medium) }>

      <menu className={ css.headerNav }>
        {PAGES.map((page) => {
          return (
            <NavLink
              key={ page.tag }
              exact
              to={ page.route }
              className={ css.headerNav__link }
              activeClassName={ css.headerNav__link_active }
            >
              { page.text }
            </NavLink>
          );
        })}
      </menu>

    </section>
  </header>
);

export default Header;
