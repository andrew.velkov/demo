import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { NavLink } from 'react-router-dom';

import Button from 'components/Button';
import Dialog from 'components/Dialog';

import css from 'style/pages/Main';

const Items = ({ item, onClick }) => (
  <li key={ item.id } className={ css.wrap__item }>
    <h3>
      <NavLink to={ `/post/${ item.id }` }>{ item.todo_title }</NavLink>
    </h3>

    {item.todo_category && <div>
      <h4>Категория: { item.todo_category }</h4>
      {item.todo_sub_category && <h5>Подкатегория: { item.todo_sub_category }</h5>}
    </div>}

    <p>{ item.todo_desc.length >= 300 ? `${ item.todo_desc.slice(0, 300) }...` : item.todo_desc }</p>

    <div className={ cx(css.buttonGroup, css.buttonGroup_between) }>
      <ul className={ css.buttonGroup }>
        <li className={ css.buttonGroup__item }>
          <Button typeButton='raiseLink' to={ `/post/${ item.id }` } primary={ true }>Details...</Button>
        </li>
        <li className={ css.buttonGroup__item }>
          <Button typeButton='raiseLink' to={ `/post/${ item.id }/edit` } secondary={ true }>Edit Post</Button>
        </li>
      </ul>
      <div className={ css.buttonGroup__item }>
        <Dialog title='Сonfirm deletion' onClick={ onClick } secondaryClass={ true } buttonName='Delete Post'>
          <p>Press the &laquo;Cancel&raquo; button until it&lsquo;s too late :)</p>
        </Dialog>
      </div>
    </div>
  </li>
);

Items.propTypes = {
  item: PropTypes.any,
  onClick: PropTypes.func,
};

export default Items;
