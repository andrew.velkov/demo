import React, { Component } from 'react';
import cx from 'classnames';
import ReactJson from 'react-json-view';

import Header from 'components/Header';

import css from 'style/pages/Main';

import testJson from '../../../../mockups/db.json';

export default class Calc extends Component {
  render() {
    return (
      <section className={ cx(css.section, css.section_gray) }>
        <Header />

        <section className={ cx(css.wrap, css.wrap_medium) }>
          <ul className={ css.grid }>
            <li className={ cx(css.grid__item, css.grid__item_12) }>
              <div className={ css.grid__ibox }>
                <h3 className={ css.grid__title }>Info</h3>
                <div className={ css.well }>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi, accusantium.</p>
                </div>
              </div>
            </li>
            <li className={ cx(css.grid__item, css.grid__item_12) }>
              <div className={ css.grid__ibox }>
                <h3 className={ css.grid__title }>Json view</h3>
                <div className={ css.well }>
                  <ReactJson src={ testJson } />
                </div>
              </div>
            </li>
          </ul>
        </section>

      </section>
    );
  }
}
