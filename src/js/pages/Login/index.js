import React, { Component } from 'react';
import cx from 'classnames';
import Button from 'components/Button';
import Header from 'components/Header';
import Input from 'components/Form/Input';

import css from 'style/pages/Main';

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      email: '',
      password: '',
      passwordConfirmation: '',
    };
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  handleSubmit = async (e) => {
    e.preventDefault();

    this.setState({
      username: '',
      email: '',
      password: '',
      passwordConfirmation: '',
    });
  }

  render() {
    const { email, password } = this.state;

    return (
      <section className={ cx(css.section, css.section_gray) }>
        <Header />

        <section className={ cx(css.wrap, css.wrap_medium) }>
          <ul className={ css.grid }>
            <li className={ cx(css.grid__item, css.grid__item_12) }>
              <div className={ css.grid__ibox }>
                <h3 className={ css.grid__title }>Sign Up</h3>

                <form onSubmit={ this.handleSubmit }>
                  <Input label='Email *' type='text' name='email' value={ email } onChange={ (e) => this.handleChange(e) } />
                  <Input label='Password *' type='password' name='password' value={ password } onChange={ (e) => this.handleChange(e) } />
                  <div className={ css.buttonGroup }>
                    <Button type='submit' primaryClass={ true } disabledClass={ email === '' || password === '' }>Send</Button>
                  </div>
                </form>

              </div>
            </li>
          </ul>
        </section>

      </section>
    );
  }
}
