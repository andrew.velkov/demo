import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { filter } from 'lodash';
import { connect } from 'react-redux';
import ActionFavorite from 'material-ui/svg-icons/action/favorite';

import { getCalc, updatedCalc } from 'redux/reducers/calc';
import Button from 'components/Button';
import Header from 'components/Header';
import Fetching from 'components/Fetching';
import InputRange from 'components/Form/InputRange';
import Radio from 'components/Form/Radio';

import CURRENCY from 'config/currency';
import PAYMENT from 'config/payment';
import percentConfig from 'config/percentConfig';

import css from 'style/pages/Main';

@connect((state) => ({
  calc: state.calc.get,
}),
{ getCalc, updatedCalc }
)
export default class Calc extends Component {
  static propTypes = {
    calc: PropTypes.object,
    history: PropTypes.object,
    getCalc: PropTypes.func,
    updatedCalc: PropTypes.func,
  }

  constructor(props) {
    super(props);
    const { amount, term, currency, payment, rate } = this.props.calc.data;
    this.state = { amount, term, currency, payment, rate };
  }

  componentDidMount() {
    this.props.getCalc();
  }

  componentWillReceiveProps(nextProps) {
    this.init(nextProps);
  }

  getPercent = (term, currencyValue, payment) => {
    filter(percentConfig, (data) => {
      const { monthlyPayment, finallyPayment } = data.percent;
      const paymets = payment !== PAYMENT[0].value ? finallyPayment.currency : monthlyPayment.currency;
      const percent = currencyValue !== CURRENCY[0].value ? paymets.uah : paymets.usd;

      if (+term >= data.period.start && +term <= data.period.end) {
        this.setState({
          rate: percent,
        });
      }
    });
  }

  init = (props) => {
    const { calc: { data, loaded } } = props;

    if (loaded) {
      this.setState({
        amount: data.amount,
        term: data.term,
        currency: data.currency,
        payment: data.payment,
        rate: data.rate,
      });
    }
  }

  pow = (x, n) => {
    let result = x;
    for (let i = 1; i < n; i++) {
      result *= x;
    }
    return result;
  }

  handleChange = async (e) => {
    await this.setState({
      [e.target.name]: e.target.value,
    });
    this.calcPercent();
  }

  calcPercent = () => {
    const { term, currency: currencyValue, payment } = this.state;
    this.getPercent(term, currencyValue, payment);
  };

  handleSubmit = async (e, totals) => {
    e.preventDefault();
    const data = Object.assign(this.state, totals);

    await this.props.updatedCalc(data).then(() => {
      this.props.history.push('/calc/view');
    }).catch((error) => error);
  }

  render() {
    const { loading } = this.props.calc;
    const { amount, term, currency, payment, rate } = this.state;

    const fullTimeIncome = payment === PAYMENT[1].value ? Math.ceil((amount * this.pow(1 + ((rate / 100) / 12), term)) - amount) : Math.ceil((((amount * rate) / 12) * term) / 100);
    const monthlyIncome = payment === PAYMENT[1].value ? Math.ceil(fullTimeIncome / term) : Math.ceil(((amount * rate) / 100) / 12);
    const currencyCode = currency === CURRENCY[0].value ? '$' : '₴';

    return (
      <section className={ cx(css.section, css.section_gray) }>
        <Header />

        <section className={ cx(css.wrap, css.wrap_medium) }>
          <ul className={ css.grid }>
            <li className={ cx(css.grid__item, css.grid__item_8) }>
              <div className={ css.grid__ibox }>
                <h3 className={ css.grid__title }>Calc</h3>

                <Fetching isFetching={ loading } size={ 45 } thickness={ 8 }>
                  <form onSubmit={ (e) => this.handleSubmit(e, { fullTimeIncome, monthlyIncome, currencyCode }) }>
                    <Radio
                      data={ CURRENCY }
                      name='currency'
                      valueSelected={ currency }
                      onChange={ (e) => this.handleChange(e) }
                      checkedIcon={ <ActionFavorite /> }
                    /><hr />

                    <InputRange
                      label='Сумма'
                      name='amount'
                      min={ 1000 }
                      max={ 100000 }
                      step={ 1000 }
                      propValue={ currencyCode }
                      value={ amount }
                      onChange={ (e) => this.handleChange(e) }
                    /><hr />

                    <InputRange
                      label='Срок'
                      name='term'
                      min={ 3 }
                      max={ 36 }
                      step={ 1 }
                      propValue='мес'
                      value={ term }
                      onChange={ (e) => this.handleChange(e) }
                    /><hr />

                    <Radio
                      data={ PAYMENT }
                      name='payment'
                      valueSelected={ payment }
                      onChange={ (e) => this.handleChange(e) }
                    /><hr />

                    <div>
                      <p>Процентная ставка: <b>{ rate }%</b> <small> - «зависит от срока, валюты и формата выплаты процентов»</small></p>
                    </div><hr />

                    <Button type='submit' primaryClass={ true }>Инвестировать</Button>
                  </form>
                </Fetching>
              </div>
            </li>

            <li className={ cx(css.grid__item, css.grid__item_4) }>
              <div className={ css.grid__ibox }>
                <h3 className={ css.grid__title }>View</h3>
                <p>Валюта: <b>{ currency }</b></p>
                <p>Сумма: <b>{ amount }</b></p>
                <p>Срок: <b>{ term } мес</b></p>
                <p>Выплата процентов: <b>{ payment }</b></p>
                <p>Процентная ставка: <b>{ rate }%</b></p>
                <hr />
                <p><b>Ваш пасивный доход:</b><br /></p>
                <p>Ежемесячно: <b>{ monthlyIncome }{ currencyCode }</b></p>
                <p>За весь срок вложения: <b>{ fullTimeIncome }{ currencyCode }</b></p>
              </div>
            </li>
          </ul>
        </section>
      </section>
    );
  }
}
