import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { connect } from 'react-redux';
import { orderBy } from 'lodash';

import { getPost, createPost, removePost } from 'redux/reducers/posts';
import { Input, Select, Radio } from 'components/Form';
import Header from 'components/Header';
import Items from 'components/Items';
import Dialog from 'components/Dialog';
import Fetching from 'components/Fetching';
import Tabs from 'components/Tabs';
import Table from 'components/Table';
import Pagination from 'components/Pagination';

import CATEGORIES from 'config/categories';
import COLUMNS from 'config/columns';

import css from 'style/pages/Main';

const invertDirection = {
  'asc': 'desc',
  'desc': 'asc',
};

@connect((state) => ({
  posts: state.posts.get,
}),
{ getPost, createPost, removePost }
)
export default class Main extends Component {
  static propTypes = {
    posts: PropTypes.object,
    getPost: PropTypes.func,
    createPost: PropTypes.func,
    removePost: PropTypes.func,
  }

  constructor(props) {
    super(props);

    this.state = {
      todoTitle: '',
      todoDesc: '',
      todoUrl: '',
      selectedCategory: null,
      selectedSubCategory: null,
      catalog: [],
      search: '',
      radio: '',
      editIdx: -1,
      columnToSort: '',
      sortDirection: 'desc',
    };
  }

  componentDidMount() {
    this.props.getPost();
  }

  onChangeCategory = (event, index, value) => {
    this.setState({
      selectedCategory: value.id,
      selectedSubCategory: '',
      catalog: value.catalog,
    });
  }

  onChangeSubCategory = (event, index, value) => {
    this.setState({
      selectedSubCategory: value.id,
    });
  }

  setPagination = (selectedPage) => {
    this.props.getPost(selectedPage);
  };

  filtrationArticles = (search) => {
    return data => {
      if (this.state.radio) {
        return data.todo_category.toLowerCase().includes(this.state.radio.toLowerCase()) || !this.state.radio;
      }

      return data.todo_title.toLowerCase().includes(search.toLowerCase()) || data.todo_desc.toLowerCase().includes(search.toLowerCase()) || !search;
    };
  }

  handleChange = (event) => {
    if (this.state.search) {
      this.setState({
        radio: '',
      });
    }

    this.setState({
      [event.target.name]: event.target.value,
    });
  }

  handleSort = (columnName) => {
    this.setState(state => ({
      columnToSort: columnName,
      sortDirection: state.columnToSort === columnName ? invertDirection[state.sortDirection] : 'asc',
    }));
  }

  handleSubmit = async (event) => {
    event.preventDefault();

    const { selectedCategory, selectedSubCategory } = this.state;
    const categories = CATEGORIES.find((item) => item.id === selectedCategory);
    const subCategories = categories && categories.catalog.find((item) => item.id === selectedSubCategory);

    const dataPost = {
      todo_title: this.state.todoTitle.trim(),
      todo_desc: this.state.todoDesc.trim(),
      todo_url: this.state.todoUrl.trim(),
      todo_category: categories !== undefined ? categories.value : '',
      todo_sub_category: subCategories !== undefined ? subCategories.value : '',
    };

    if (this.state.todoTitle !== '') {
      await this.props.createPost(dataPost);
      this.props.getPost();
    }

    this.setState({
      todoTitle: '',
      todoDesc: '',
      todoUrl: '',
      selectedCategory: null,
    });
  }

  handleDelete = async (id) => {
    await this.props.removePost(id);
    this.props.getPost();
  }

  listPost = () => {
    const { data, loading, loaded } = this.props.posts;
    const { search } = this.state;

    return (
      <div className={ css.grid__ibox }>
        <Fetching isFetching={ loading } size={ 45 } thickness={ 8 }>
          <ul>
            {data.filter(this.filtrationArticles(search)).reverse().map((item) => {
              return (
                <Items item={ item } onClick={ () => this.handleDelete(item.id) } />
              );
            })}
          </ul>
        </Fetching>

        {((loaded || loading) && data.filter(this.filtrationArticles(search)).length) <= 0 && <div className={ css.well }>
          {!loading && <p>No articles...</p>}
        </div>}
      </div>
    );
  }

  tablePost = () => {
    const { data, loading, loaded } = this.props.posts;
    const { editIdx, columnToSort, sortDirection, search } = this.state;

    return (
      <div className={ css.grid__ibox }>
        <Fetching isFetching={ loading } size={ 45 } thickness={ 8 }>
          {data.filter(this.filtrationArticles(search)).length >= 1 &&
            <Table
              editIdx={ editIdx }
              dataHead={ COLUMNS }
              dataBody={ orderBy(data.filter(this.filtrationArticles(search)), columnToSort, sortDirection) }
              sortBy={ this.handleSort }
              onClick={ this.handleDelete }
            />}
        </Fetching>

        {((loaded || loading) && data.filter(this.filtrationArticles(search)).length) <= 0 && <div className={ css.well }>
          {!loading && <p>No articles...</p>}
        </div>}
      </div>
    );
  }

  render() {
    const { data } = this.props.posts;
    const { todoTitle, todoDesc, todoUrl, search, selectedCategory, selectedSubCategory, catalog } = this.state;

    return (
      <section className={ cx(css.section, css.section_gray) }>
        <Header />

        <section className={ cx(css.wrap, css.wrap_medium) }>
          <ul className={ cx(css.grid, css.grid_top) }>
            <li className={ cx(css.grid__item, css.grid__item_3) }>
              <ul className={ css.grid }>
                <li className={ cx(css.grid__item, css.grid__item) }>
                  <div className={ css.grid__ibox }>
                    <Dialog title='Add article' slogan={ true } onClick={ this.handleSubmit } disabled={ todoTitle === '' } primaryClass={ true }>
                      <form>
                        <ul className={ css.grid }>
                          <li className={ cx(css.grid__item, css.grid__item_6) }>
                            <Input name='todoTitle' label='Title *' value={ todoTitle } onChange={ (event) => this.handleChange(event) } />
                          </li>
                          <li className={ cx(css.grid__item, css.grid__item_6) }>
                            <Input name='todoUrl' label='Image URL' value={ todoUrl } onChange={ (event) => this.handleChange(event) } />
                          </li>
                          <li className={ cx(css.grid__item) }>
                            <Select data={ CATEGORIES } label='Category' value={ selectedCategory } handleChange={ this.onChangeCategory } />
                          </li>
                          {selectedCategory && <li className={ cx(css.grid__item, css.grid__item_6) }>
                            <Select data={ catalog } label='Subсategory' value={ selectedSubCategory } handleChange={ this.onChangeSubCategory } />
                          </li>}
                          <li className={ cx(css.grid__item, css.grid__item_12) }>
                            <Input name='todoDesc' label='Description' value={ todoDesc } onChange={ (event) => this.handleChange(event) } multiLine={ true } />
                          </li>
                        </ul>
                      </form>
                    </Dialog>
                  </div>
                </li>

                <li className={ css.grid__item }>
                  <div className={ css.grid__ibox }>
                    <h3 className={ css.grid__title }>Search...</h3>
                    <Input name='search' label='Search...' value={ search } onChange={ (event) => this.handleChange(event) } />
                  </div>
                </li>

                <li className={ css.grid__item }>
                  <div className={ css.grid__ibox }>
                    <h3 className={ css.grid__title }>Filters...</h3>
                    <Radio data={ CATEGORIES } name='radio' onChange={ (event) => this.handleChange(event) } />
                  </div>
                </li>
              </ul>
            </li>

            <li className={ cx(css.grid__item, css.grid__item_9) }>
              <Tabs title={ ['List Post', 'Table Post'] }>
                { [this.listPost(), this.tablePost()] }
              </Tabs>

              {(data.length > 0) && <div className={ css.grid__ibox }>
                <Pagination
                  pageCount={ data.length }
                  handleSetPage={ this.setPagination }
                />
              </div>}
            </li>
          </ul>
        </section>
      </section>
    );
  }
}
