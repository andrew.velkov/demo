const LOGIN = 'todo/LOGIN';
const LOGIN_SUCCESS = 'todo/LOGIN_SUCCESS';
const LOGIN_ERROR = 'todo/LOGIN_ERROR';

const LOGOUT = 'todo/LOGOUT';
const LOGOUT_SUCCESS = 'todo/LOGOUT_SUCCESS';
const LOGOUT_ERROR = 'todo/LOGOUT_ERROR';

const LOAD_PROFILE = 'todo/LOAD_PROFILE';
const LOAD_PROFILE_SUCCESS = 'todo/LOAD_PROFILE_SUCCESS';
const LOAD_PROFILE_ERROR = 'todo/LOAD_PROFILE_ERROR';

const SIGNUP = 'todo/SIGNUP';
const SIGNUP_SUCCESS = 'todo/SIGNUP_SUCCESS';
const SIGNUP_ERROR = 'todo/SIGNUP_ERROR';

const initialState = {

  login: {
    loading: false,
    loaded: false,
    error: {},
    data: {},
  },
  logout: {
    loading: false,
    loaded: false,
    error: {},
    data: {},
  },
  profile: {
    loading: false,
    loaded: false,
    finished: false,
    isError: false,
    error: {},
    data: {},
  },
  signup: {
    loading: false,
    loaded: false,
    error: {},
    data: {},
  },
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        login: {
          ...state.login,
          loading: true,
          loaded: false,
        },
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        login: {
          ...state.login,
          loading: false,
          loaded: true,
          data: action.payload.result,
        },
      };
    case LOGIN_ERROR:
      return {
        ...state,
        login: {
          ...state.login,
          loading: false,
          loaded: false,
        },
      };
    case LOGOUT:
      return {
        ...state,
        logout: {
          loading: true,
          loaded: false,
        },
      };
    case LOGOUT_SUCCESS:
      return {
        ...state,
        logout: {
          loading: false,
          loaded: true,
          data: action.payload.result,
        },
      };
    case LOGOUT_ERROR:
      return {
        ...state,
        logout: {
          loading: false,
        },
      };
    case LOAD_PROFILE:
      return {
        ...state,
        profile: {
          ...state.profile,
          loading: true,
          finished: false,
          isError: false,
          error: {},
        },
      };
    case LOAD_PROFILE_SUCCESS:
      return {
        ...state,
        profile: {
          ...state.profile,
          loading: false,
          loaded: true,
          finished: true,
          data: action.result,
        },
      };
    case LOAD_PROFILE_ERROR:
      return {
        ...state,
        profile: {
          ...state.profile,
          loading: false,
          finished: true,
          isError: true,
          error: action.error,
        },
      };
    case SIGNUP:
      return {
        ...state,
        signup: {
          ...state.signup,
          loading: true,
          isError: false,
          error: {},
        },
      };
    case SIGNUP_SUCCESS:
      return {
        ...state,
        signup: {
          ...state.signup,
          loading: false,
          loaded: true,
          finished: true,
          data: action.result,
        },
      };
    case SIGNUP_ERROR:
      return {
        ...state,
        signup: {
          ...state.signup,
          loading: false,
          finished: true,
          isError: true,
          error: action.error,
        },
      };
    default:
      return state;
  }
}

export const login = (data) => {
  return {
    types: [LOGIN, LOGIN_SUCCESS, LOGIN_ERROR],
    request: {
      method: 'POST',
      url: '/api/login',
      body: data,
    },
  };
};

export const logout = () => {
  return {
    types: [LOGOUT, LOGOUT_SUCCESS, LOGOUT_ERROR],
    request: {
      url: '/api/logout',
    },
  };
};

export function getAccount() {
  return {
    types: [LOAD_PROFILE, LOAD_PROFILE_SUCCESS, LOAD_PROFILE_ERROR],
    request: {
      url: '/api/user',
    },
  };
}

export const signUp = (data) => {
  return {
    types: [SIGNUP, SIGNUP_SUCCESS, SIGNUP_ERROR],
    request: {
      method: 'POST',
      url: '/api/list',
      body: data,
    },
  };
};
