import { combineReducers } from 'redux';

import account from './account';
import posts from './posts';
import calc from './calc';

const allReducers = combineReducers({
  account,
  posts,
  calc,
});

export default allReducers;
