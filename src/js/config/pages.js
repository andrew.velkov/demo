const PAGES = [
  {
    tag: 'main',
    text: 'Main',
    route: '/',
  }, {
    tag: 'calc',
    text: 'Calc',
    route: '/calc',
  },
  {
    tag: 'json',
    text: 'Json View',
    route: '/jsonview',
  }, {
    tag: 'login',
    text: 'Sign In',
    route: '/login',
  },
];

export default PAGES;
