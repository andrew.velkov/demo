const percentConfig = [
  {
    period: {
      start: 3,
      end: 5,
    },
    percent: {
      monthlyPayment: {
        currency: {
          usd: 12,
          uah: 21,
        },
      },
      finallyPayment: {
        currency: {
          usd: 13,
          uah: 22,
        },
      },
    },
  },
  {
    period: {
      start: 6,
      end: 11,
    },
    percent: {
      monthlyPayment: {
        currency: {
          usd: 16,
          uah: 23,
        },
      },
      finallyPayment: {
        currency: {
          usd: 17,
          uah: 24,
        },
      },
    },
  },
  {
    period: {
      start: 12,
      end: 23,
    },
    percent: {
      monthlyPayment: {
        currency: {
          usd: 18,
          uah: 25,
        },
      },
      finallyPayment: {
        currency: {
          usd: 20,
          uah: 27,
        },
      },
    },
  },
  {
    period: {
      start: 24,
      end: 35,
    },
    percent: {
      monthlyPayment: {
        currency: {
          usd: 19,
          uah: 28,
        },
      },
      finallyPayment: {
        currency: {
          usd: 21,
          uah: 30,
        },
      },
    },
  },
  {
    period: {
      start: 36,
      end: 37,
    },
    percent: {
      monthlyPayment: {
        currency: {
          usd: 20,
          uah: 31,
        },
      },
      finallyPayment: {
        currency: {
          usd: 22,
          uah: 33,
        },
      },
    },
  },
];

export default percentConfig;
