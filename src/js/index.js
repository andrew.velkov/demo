import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import Routes from 'routes';
import store from 'redux/store';

import 'style/main';

// Render it to DOM
ReactDOM.render(
  <Provider store={ store } aRandomProps='asdad'>
    <Routes />
  </Provider>,
  document.getElementById('root')
);
