const jsonServer = require('json-server');
const server = jsonServer.create();
const path = require('path');
const qs = require('query-string');
const defaultRoutes = require('./routes.js');
const router = jsonServer.router(path.join(__dirname, 'db.json'));
const middlewares = jsonServer.defaults();

const requestDelay = 1;
const privateURL = ['/user'];

server.use(middlewares);
server.use(jsonServer.bodyParser);

server.use((req, res, next) => {
  setTimeout(next, requestDelay * 1000);
});

server.use((req, res, next) => {
  if (isPrivate(req)) {
    if (isAuthorized(req)) {
      next();
    } else {
      res.sendStatus(401);
    }
  } else {
    if(isLogin(req)) {
      setAuthorized(req, res);
    }
    if(isLogout(req)) {
      deleteAuthorized(req, res);
    }

    next();
  }
});

server.use((req, res, next) => {
  if (req.method === 'PATCH' || req.method === 'PUT') {
    delete req.body.id;
  }

  next();
});


router.render = (req, res) => {
  const params = qs.parse(req._parsedUrl.query);

  if (params.page && params.limit) {
    const available = res.locals.data.length;
    const pageCounts = Math.ceil(available / params.limit);

    res.jsonp({
      "status": "success",
      "success": {
        "code": 200
      },
      "result": {
        "count" : +params.limit,
        "pageindex" : +params.page,
        "data": res.locals.data.splice((params.page - 1) * params.limit, params.limit),
        "pagecount": pageCounts,
        "available": available
      }
    });
  } else {
    res.jsonp({
      "status": "success",
      "success": {
        "code": 200
      },
      "result": res.locals.data
    });
  }
};

// Use default router
server.use(jsonServer.rewriter(defaultRoutes));

//Use default database
server.use(router);

server.listen(3013, () => {
  console.log('JSON Server is running')
});

function isAuthorized(req) {
  return req.headers && req.headers.cookie && (req.headers.cookie.includes('SESSION_MOCKUPS') && !req.headers.cookie.includes('SESSION_MOCKUPS=0'));
}

function setAuthorized(req, res) {
  req.method = 'GET';
  res.cookie('SESSION_MOCKUPS', Date.now());
  req.query = req.body;
  return req;
}

function deleteAuthorized(req, res) {
  req.method = 'GET';
  res.cookie('SESSION_MOCKUPS', 0);
  req.query = req.body;
  return req;
}

function isPrivate(req) {
  return privateURL.some((url) => req.url.includes(url));
}

function isLogin(req) {
  return  req.url.indexOf('/login') >= 0;
}

function isLogout(req) {
  return req.url.indexOf('/logout') >= 0;
}
